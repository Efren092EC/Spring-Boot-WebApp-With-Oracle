package demoapp.springbootframework.controller;

import demoapp.springbootframework.model.Cliente;
import demoapp.springbootframework.services.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ClientesController {

    private ClientesService clientesService;

    @Autowired
    public void setClientesService(ClientesService clientesService) {
        this.clientesService = clientesService;
    }

    @RequestMapping(value = "/clientes", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("clientes", clientesService.listAllclientes());
        return "clientes";
    }

    @RequestMapping("clientes/{id}")
    public String showCliente(@PathVariable Integer id, Model model) {
        model.addAttribute("cliente", clientesService.getByidCliente(id));
        return "clienteshow";
    }

    @RequestMapping("clientes/edit/{id}")
    public String edit(@PathVariable Integer id, Model model) {
        model.addAttribute("cliente", clientesService.getByidCliente(id));
        return "clientesform";
    }

    @RequestMapping("clientes/new")
    public String newCliente(Model model) {
        model.addAttribute("Cliente", new Cliente());
        return "clienteform";
    }

    @RequestMapping(value = "cliente", method = RequestMethod.POST)
    public String saveProduct(Cliente cliente) {
        clientesService.saveCliente(cliente);
        return "redirect:/cliente/" + cliente.getIdCliente();
    }

    @RequestMapping("cliente/delete/{id}")
    public String delete(@PathVariable Integer id) {
        clientesService.deleteCliente(id);
        return "redirect:/clientes";
    }

}
