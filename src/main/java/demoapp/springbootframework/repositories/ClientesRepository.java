package demoapp.springbootframework.repositories;

import demoapp.springbootframework.model.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClientesRepository extends CrudRepository<Cliente, Integer>{
}
