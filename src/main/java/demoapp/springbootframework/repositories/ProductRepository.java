package demoapp.springbootframework.repositories;

import demoapp.springbootframework.model.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Integer>{
}
