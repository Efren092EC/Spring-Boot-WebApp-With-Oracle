package demoapp.springbootframework.services;


import demoapp.springbootframework.model.Cliente;

public interface ClientesService {
    
    Iterable<Cliente> listAllclientes();

    Cliente getByidCliente(Integer idCliente);

    Cliente saveCliente(Cliente cliente);

    void deleteCliente(Integer idCliente);
}
