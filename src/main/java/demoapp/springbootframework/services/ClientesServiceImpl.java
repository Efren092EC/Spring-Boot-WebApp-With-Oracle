package demoapp.springbootframework.services;

import demoapp.springbootframework.model.Cliente;
import demoapp.springbootframework.repositories.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientesServiceImpl implements ClientesService {

    private ClientesRepository clientesRepository;

    @Autowired
    public void setProductRepository(ClientesRepository clientesRepository) {
        this.clientesRepository = clientesRepository;
    }

    @Override
    public Iterable<Cliente> listAllclientes() {
        return clientesRepository.findAll();
    }

    @Override
    public Cliente getByidCliente(Integer idCliente) {
        return clientesRepository.findOne(idCliente);
    }

 
    @Override
    public void deleteCliente(Integer idCliente) {
        clientesRepository.delete(idCliente);
    }

    @Override
    public Cliente saveCliente(Cliente cliente) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
